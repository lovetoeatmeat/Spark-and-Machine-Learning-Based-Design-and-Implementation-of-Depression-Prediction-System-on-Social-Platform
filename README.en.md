# 基于Spark和机器学习的社交平台抑郁症人群预测系统的设计与实现

#### 一、介绍

基于Spark和机器学习的社交平台抑郁症人群预测系统的设计与实现

#### 题目来源

自定。在新冠疫情长期封闭居家后，抑郁症人群在世界范围都呈现急剧上升趋势，由此对疫情期间作为人们的主要情感抒发渠道的社交平台进行情感分析，对预测潜在抑郁症人群及其变化趋势有一定的研究价值和意义。


   #### 研究背景

   一方面，据世界卫生组织统计，全世界有超过3.5亿人受抑郁症困扰，其已成为世界第四大疾病和人类第二大杀手。且在新冠疫情长期封闭居家后，抑郁症人群在世界范围又呈现急剧上升趋势，新冠第一年，抑郁症人数飙升 25%，重度抑郁症增加了28%。亟待投入更多的资源对抑郁症人群提供帮助与治疗。

   另一方面，研究表明，抑郁症患者社会功能受损明显，但多数抑郁症患者未到专业机构寻求帮助，因此仅有0.5%的患者得到了充分治疗，这就使得我们需要用主动预测的方式寻找潜在的抑郁症人群以及时提供帮助。

   并且，社交平台正在成为人们生活中不可或缺的组成要素，它们反映了用户的个人生活，理论上可以提供较为可靠的预测依据。

   由此，对一款以社交平台作为数据源，大数据技术作为支撑的抑郁症预测系统的需求便产生了，这也是本系统设计构想的由来。

   #### 研究的目的和意义

   通过外界手段进行预测和提醒，以帮助抑郁症患者尽早发现可能存在的问题。此时承载其日常生活记录和情感抒发的社交平台无疑是最直接也是最有价值的分析来源，通过借助机器学习算法对其社交平台发文进行情感分析，就有可能更早地发现其心理健康问题，从而尽早提醒这些潜在患者进行治疗，这也是本项目的研究目的和意义所在。

   #### 二、开发环境

   本系统整体采用Docker思想进行开发，因此各模块使用了不同的开发语言和开发环境，本部分主要进行各模块开发环境的介绍。

   本系统按功能共设计为四个模块，每个模块逻辑上独立地负责所要实现的功能，具体为数据获取模块，数据存储模块，数据处理模块和数据可视化模块，整体功能结构设计如图所示。

   ![](https://treathy.com/wp-content/uploads/2024/03/Spark-and-Machine-Learning-Based-Design-and-Implementation2.1.png)

   #### 数据获取模块

   •软件环境：Ubuntu系统

   •开发环境：Python3.8+Jupyter Notebook

   #### 数据存储模块

   •软件环境：Ubuntu系统

   •开发环境：MySQL8

   #### 数据处理模块

   •软件环境：Ubuntu系统

   •开发环境：Python3.8+Jupyter Notebook+jdk8+Spark2.3+Hadoop2.8

   #### 数据可视化模块

   •软件环境：Ubuntu系统

   •开发环境： Python3.8+Jupyter Notebook+jdk17+Tomcat9+Eclipse

   #### 三、实现功能

   本部分主要介绍本系统实现的功能

   本系统按功能共设计为四个模块，具体实现的功能如图所示，其中数据获取模块实现的功能可归纳为1.1爬取原始发文数据，1.2数据预处理，1.3爬取用户信息；数据处理模块实现的功能可归纳为1.4数据标签化处理；数据可视化模块实现的功能可归纳为1.6前端展示，2.1查看可视化图表。2.2数据导出下载。

   ![](https://treathy.com/wp-content/uploads/2024/03/Spark-and-Machine-Learning-Based-Design-and-Implementation3.1.png)

   #### 整体流程

   ![](https://treathy.com/wp-content/uploads/2024/03/Spark-and-Machine-Learning-Based-Design-and-Implementation3.2.png)

   #### 得出的结论

   ![image-20231228154829065](https://treathy.com/wp-content/uploads/2024/03/Spark-and-Machine-Learning-Based-Design-and-Implementation3.3.png)

   #### 四、关键技术

   本部分主要对各模块所涉及的关键技术进行介绍

   #### 数据获取模块 Scrapy框架

   ![](https://treathy.com/wp-content/uploads/2024/03/Spark-and-Machine-Learning-Based-Design-and-Implementation4.1.jpg)

   数据获取模块主要负责数据的爬取，以原始发文数据爬取部分为代表，该部分使用Scrapy框架。Scrapy是一个框架，可以根据需要十分简便地进行参数修改。在本系统中通过对settings.py进行配置，来实现不同关键词，不同时间范围的针对性爬取。

   实际使用中，受限于IP数量，仅支持进行一天三组关键词或三段指定时间范围的爬取，为了使样本集中，实际爬取选用的关键词多为带有负面情感倾向的词汇。

   基于Spark和机器学习的社交平台抑郁症人群预测系统的设计与实现

   #### 数据存储模块 MySQL数据库

   ![](https://treathy.com/wp-content/uploads/2024/03/Spark-and-Machine-Learning-Based-Design-and-Implementation4.2.png)

   数据存储模块采用MySQL8作为数据库，共设有原始发文数据“rawTextTb”表和用户信息“summary”表两张表。数据存储模块负责整个系统中的数据交互，为提高效率，与数据获取模块和数据处理模块交互采用PooledDB连接池，与数据可视化模块交互则同时采用PooledDB连接池和Dbcp连接池。

   数据量方面，截至论文定稿，系统共存入约16.2万条原始发文数据和约8万条用户信息。

   #### 数据处理模块 SnowNLP算法

   ![image-20231228155456058](https://treathy.com/wp-content/uploads/2024/03/Spark-and-Machine-Learning-Based-Design-and-Implementation4.3.png)

   本系统讨论的问题可视为一个贝叶斯模型（Bayes）二分类问题，即是否为潜在抑郁症，分别设为c1类和c2类，假设训练样本只有两个特征x和y，则对贝叶斯模型（Bayes）来说，模型的预测即计算P(c1|x,y)和P(c2|x,y)，而P(ci|x,y)可表示为公式 (1)，如果P(c1│x,y)>P(c2|x,y)，则当前样本属于c1类，反之属于c2类。

   本系统使用的SnowNLP模型就是基于此公式进行训练的，首先运行train()函数进行分词，对特征的出现频次进行统计，再运行classify()函数进行贝叶斯模型的预测，实际的函数是公式(1)在二分类多特征条件下的推导，即公式(2)，用于训练的正负样本总量共178万行，大小共224MB。

   #### 数据处理模块 Spark集群

   ![](https://treathy.com/wp-content/uploads/2024/03/Spark-and-Machine-Learning-Based-Design-and-Implementation4.4.png)

   系统实际搭建时为数据处理模块搭建有Hadoop+Spark集群，共分配七个独立容器，Spark集群采用Spark on Yarn模式搭建，对数据获取模块爬取到的原始发文数据进行基于SnowNLP模型的情感分析，并依据其结果值对原始发文数据进行标签化。

   实际处理过程并未每一次都在Spark内进行模型训练，而是预先进行模型训练，在Spark处理时只引用训练好的模型进行情感分析，从而使得Spark处理部分的时间开销主要由与数据库交互的速度限制，进一步提升了效率。

   运行时采用的是client 模式，以减小cluster模式多并发线程带来的硬件消耗。

   #### 数据可视化模块 PyEcharts+Java Web

   ![](https://treathy.com/wp-content/uploads/2024/03/Spark-and-Machine-Learning-Based-Design-and-Implementation4.5.png)

   数据可视化模块采用Python和Java结合的方式对汇总数据进行可视化展现，实际分为数学图表绘制和前端展示两部分。

   其中数学图表绘制部分采用Python编写，利用PyEcharts进行汇总和当日图表的绘制，并以Html文件的形式储存到前端展示部分Tomcat的项目目录下。

   前端展示部分采用Java编写，负责搭建前端平台展示绘制的数学图表和数据库“summary”表中的汇总用户信息，并提供数据导出功能。

   #### 系统整体 Docker容器技术

   本系统整体采用Docker容器思想，Docker技术允许为每一个模块分配独立的容器，使各模块在实际运行过程中相互独立，模块间不需考虑各模块内部实际代码运行逻辑，只需考虑模块实际的输入输出文件，故即便本系统各模块采取了不同的编程语言，仍能稳定协调运行。同时Docker技术允许各多容器挂载宿主机内同一数据卷，因此可以实现通畅的数据同步和数据交互。

   #### 五、系统特色

   本部分主要对系统的特色进行介绍，突出与同类系统的不同之处

   #### SnowNLP模型的自定义训练

   ![](https://treathy.com/wp-content/uploads/2024/03/Spark-and-Machine-Learning-Based-Design-and-Implementation5.1.png)

   ![](https://treathy.com/wp-content/uploads/2024/03/Spark-and-Machine-Learning-Based-Design-and-Implementation5.2.png)

   ![](https://treathy.com/wp-content/uploads/2024/03/Spark-and-Machine-Learning-Based-Design-and-Implementation5.3.png)

   ![](https://treathy.com/wp-content/uploads/2024/03/Spark-and-Machine-Learning-Based-Design-and-Implementation5.4.png)

   （1）训练数据量大：本系统的SnowNLP模型为自定义训练，训练集样本包含正负样本总量共178万行，实际大小共224MB，实际参与数据处理的原始发文数据量同样达到了16.2万条。

   （2）用jieba中的lcut方法替换snownlp中的seg分词方法，提高分词准确率。

   （3）设置自定义词库，提高对抑郁症常见特征词的分词准确度。

   （4）对于长文本，借助jieba.analyse.textrank()方法先进行关键提取词或文本摘要抽取，再提取关键词，从而准确识别长文本中心词，提高语义判别准确度。

   #### 完整的Spark集群

   ![](https://treathy.com/wp-content/uploads/2024/03/Spark-and-Machine-Learning-Based-Design-and-Implementation5.5.png)

   得益于Docker技术，系统为数据处理模块实际搭建有Hadoop+Spark集群，共分配了七个Docker容器，采用了Spark on YARN的部署模式。

   相比于单机standalone部署模式，本系统具有更高的运算速度，同时Yarn的资源分配更加细致，使得系统可以实现更高效的资源分配，从而实现资源弹性管理。

   相比于使用多个虚拟机进行集群部署，本系统借助Docker的设计理念，使得各容器间虽然逻辑上相互独立，而实际上共用了相同的底层操作系统，这极大降低了对硬件的要求，以更小的硬件消耗带来了比多虚拟机集群部署更高的系统性能。

   #### 整体采用Docker思想

   ![](https://treathy.com/wp-content/uploads/2024/03/Spark-and-Machine-Learning-Based-Design-and-Implementation5.6.png)

   本系统整体采用Docker容器思想，为每一个模块分配独立的容器，共包含10个独立容器，且各模块在实际运行过程中相互独立，同时Docker技术允许各多容器挂载宿主机内同一数据卷，因此理论上可以实现通畅的数据同步和数据交互。除了上文提到的，Docker还赋予了本系统以下两个优势。

   （1）由于系统内所有容器挂载了宿主机内同一数据卷，而该数据卷包含各模块的实际执行代码，因此可以直接在宿主机内对各模块执行代码进行更新，且由于各模块在实际运行过程中相互独立，因此更新过程中不需要关停系统其他模块，具有高可扩展性和高可维护性。

   （2）在将本系统上传到Docker Hub仓库后，得益于Docker容器支持由Dockerfile生成的特性，本系统可以实现单行命令进行整个项目的跨系统部署，这意味着在部署本系统时不需要考虑任何的环境配置，只需要一行命令，即可在等待后完成整个系统10个容器的部署，并可直接使用。

   #### 多语言协同

   本系统在初期设计时，就考虑希望能够使用多种语言，使各门语言各自发挥其长处。在实际实现时，也做到了同时使用Python和Java两种语言， Python擅长于数据爬取和可视化绘图，于是这两部分就由Python编写，Java擅长于平台搭建，于是这部分就由Java编写。在后续改进过程中，实际上还可以将数据处理模块的Python语言更换为Scala语言，以发挥Scala语言在Spark处理方面的高效性，进一步优化系统效率。

   #### 服务器部署

   本系统整体完成了端口映射和服务器部署，可通过浏览器直接访问数据可视化模块；也可通过22端口SSH连接所有容器，从而与本地进行直接的数据交互；此外还可直接访问Hadoop和Spark的各个web监控页面，查看资源分配和数据处理过程。
